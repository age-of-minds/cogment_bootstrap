# client.py
import cog_settings

from data_pb2 import HumanAction
from cogment.client import Connection

# Create a connection to the Orchestrator serving this project
conn = Connection(cog_settings, "orchestrator:9000")

# Initiate a trial
trial = conn.start_trial(cog_settings.actor_classes.human)

# Perform actions, and get observations
observation = trial.do_action(HumanAction())
observation = trial.do_action(HumanAction())
observation = trial.do_action(HumanAction())
observation = trial.do_action(HumanAction())

# cleanup
trial.end()

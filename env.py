import cog_settings
from data_pb2 import Observation

from cogment import Environment, GrpcServer


class Env(Environment):
    def start(self, config):
        print("environment starting")
        return Observation()

    def update(self, actions):
        print("environment updating")
        return Observation()

    def end(self):
        print("environment end")
        return Observation()


if __name__ == "__main__":
    server = GrpcServer(Env, cog_settings)
    server.serve()

import cog_settings
from data_pb2 import AgentAction

from cogment import Agent, GrpcServer


class AgentImpl(Agent):
    actor_class = cog_settings.actor_classes.agent

    def decide(self, observation):
        print("agent decide")
        return AgentAction()

    def reward(self, reward):
        print("agent reward")

    def end(self):
        print("agent end")


if __name__ == '__main__':
    server = GrpcServer(AgentImpl, cog_settings)
    server.serve()
